De nos jours, les logiciels les plus utilisés dans le domaine de l’UI/UX design sont Adobe XD, Figma, Invision et Sketch. Dans cet article nous allons comparer ces divers outils et voir lequel correspond le plus aux besoins que l’on recherche.

Il y’en a trois qui reviennent souvent :

- Fournir un prototype HD avec des transitions poussées, interaction au scroll ... pour permettre à l'équipe de développement ou aux utilisateurs de visualiser au mieux le résultat attendu, avant de lancer la phase de développement.

- L'utilisation des composants pour optimiser notre projet, que ce soit pour un projet complexe (siteEcommerce, Application) ou par gain de temps

- Faciliter le travail colaboratif

Toutes les applications sont sensiblement identiques mais chacune possède leurs particularités et répondent à un besoin particulier.

## Figma

Le seul des trois accessibles partout juste avec un connexion internet. Utilisé par les designers de twitter, Microsoft, GitHub... Avantages : - Collaboration en temps réel - Basé sur internet - Rapide - Sauvegardé dans le cloud - Pas de mise à jour - Intégrateur ccs Inconvénients : - Plugins limités - Pas de mode hors-ligne - Peut être technique à prendre en main au début Prix : gratuit pour les comptes individuels, ou 12$/months

Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut mollitia sunt tempora maxime cupiditate, perspiciatis placeat non impedit recusandae molestiae nam sint omnis magni nobis, pariatur dolor quo natus reprehenderit.

## Adobe XD

Si vous êtes un adepte de la suite adobe, alors cet outils est très certainement fait pour vous, ADOBE XD étant lié aux autres applications de la suite tel que Photoshop, Illustrator… Avantages : - Facile d’utilisation - Accessible sur macOS et Windows - Puissant - Langage Français - Intégrateur css - Mode Horsligne Inconvénients : - A besoin de faire des mises à jour - Moins de plugins - Difficultés de collaborations - Animations limitées Prix : Version d’essai de 7 jours, puis 11.99$/mois

![Image Figma](../images/alexander-shatov-mr4JG4SYOF8-unsplash.jpg)

## Invision

Des prototypes avec des animations avancées pour être au plus proche du résultat voulu. Avantages : - Interface intuitive - DarkMode - Animation poussée - Accessible sur macOS et Windows - Mode Hors-ligne Inconvénients : - Peut avoir des lenteurs dû aux ressources exigé pour les animations - Difficultés de collaborations Prix : 7.95$/mois

placeat aliquid alias molestiae et modi necessitatibus quos incidunt rem vel illo? Eaque architecto quisquam, reprehenderit necessitatibus distinctio, nobis sapiente, itaque quas dicta possimus quis commodi aliquid fugiat? Sit assumenda natus quos voluptatem?

Elles m'ont permis de faire de nouvelles rencontres, d'acquérir de nouvelles connaissances et de nouvelles compétences qui me serviront dans mes futurs projets.

## Conclusion

Lorem ipsum dolor, sit amet consectetur adipisicing elit. Tempora eveniet, fugiat amet repellat nesciunt, possimus qui cum commodi, sequi officia impedit harum a. Corporis quasi eveniet perferendis hic cupiditate culpa laboriosam veritatis accusamus. Expedita, totam eos voluptate ut reprehenderit, quibusdam temporibus, quasi quisquam numquam nesciunt voluptates. Quasi iste est dolores?
